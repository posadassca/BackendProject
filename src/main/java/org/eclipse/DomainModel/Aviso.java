package org.eclipse.DomainModel;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Aviso {

	@Id
	@GeneratedValue
	private Long Id_Aviso;
	private Date fecha_vencimiento;
	private String descripcion;
	
	@ManyToOne(mappedBy = "banda", cascade = CascadeType.ALL)
	private Banda banda;
	
	@ManyToOne(mappedBy = "instrumento", cascade = CascadeType.ALL)
	private Instrumento instrumento;
	
	@ManyToMany(cascade = CascadeType.ALL)
	private ArrayList<GeneroMusical> generos_musicales = new ArrayList<GeneroMusical>();
	
	@OneToOne(cascade = CascadeType.ALL)
	private Match match;
	
	Aviso(){
		
	}

	public Aviso(Long id, Date fecha_vencimiento, String descripcion) {
		super();
		Id_Aviso = id;
		this.fecha_vencimiento = fecha_vencimiento;
		this.descripcion = descripcion;
	}

	public Long getId() {
		return Id_Aviso;
	}

	private void setId(Long id) {
		Id_Aviso = id;
	}

	public Date getFecha_vencimiento() {
		return fecha_vencimiento;
	}

	public void setFecha_vencimiento(Date fecha_vencimiento) {
		this.fecha_vencimiento = fecha_vencimiento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
