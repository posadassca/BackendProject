package org.eclipse.DomainModel;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Banda {

	@Id
	@GeneratedValue
	private Integer Id_Banda;
	private String nombre;
	private String descripcion;

	@ManyToMany (cascade = CascadeType.ALL)
	private ArrayList<Musico> musicos = new ArrayList<Musico>();

	@OneToMany(cascade = CascadeType.ALL)
	private ArrayList<Aviso> avisos = new ArrayList<Aviso>();
	
	Banda() {

	}

	public Banda(Integer id, String nombre, String descripcion) {
		super();
		Id_Banda = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return Id_Banda;
	}

	private void setId(Integer id) {
		Id_Banda = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/*
	public void agregarMusico(Musico musico){
		
		if (!musicos.contains(musico)){					
			this.musicos.add(musico);
			musico.agregarBanda(this);
		}
	}
	
	public void eliminarMusico(Musico musico){
		
		if (musicos.contains(musico)){
			this.musicos.remove(musico);
		}
	
	}
	*/
}
