package org.eclipse.DomainModel;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Ofrecimiento {

	@Id
	@GeneratedValue
	private Integer Id_Ofrecimiento;
	private String descripcion;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Match match;
	
	@ManyToMany(cascade = CascadeType.ALL)
	private ArrayList<GeneroMusical> generos = new ArrayList<GeneroMusical>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	private ArrayList<Instrumento> instrumentos = new ArrayList<Instrumento>();
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Musico musico;
	
	Ofrecimiento(){
		
	}

	public Ofrecimiento(Integer id, String descripcion) {
		super();
		Id_Ofrecimiento = id;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return Id_Ofrecimiento;
	}

	private void setId(Integer id) {
		Id_Ofrecimiento = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void agregarMusico(Musico music) {
		// TODO Auto-generated method stub
		this.musico = music;
	}
	
}
