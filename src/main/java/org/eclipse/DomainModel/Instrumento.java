package org.eclipse.DomainModel;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Instrumento {

	@Id
	@GeneratedValue
	private Long Id_Instrumento;
	private String nombre;
	
	@OneToMany(cascade = CascadeType.ALL)
	private ArrayList<Aviso> avisos = new ArrayList<Aviso>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	private ArrayList<Ofrecimiento> ofrecimientos = new ArrayList<Ofrecimiento>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	private ArrayList<Musico> musicos = new ArrayList<Musico>();
	
	Instrumento(){
		
	}

	public Instrumento(Long id, String nombre) {
		super();
		Id_Instrumento = id;
		this.nombre = nombre;
	}

	public Long getId() {
		return Id_Instrumento;
	}

	private void setId(Long id) {
		Id_Instrumento = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
