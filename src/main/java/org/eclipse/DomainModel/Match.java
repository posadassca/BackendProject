package org.eclipse.DomainModel;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Match {

	@javax.persistence.Id
	@GeneratedValue
	private Long Id_Match;
	private Date fecha;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Aviso aviso;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Ofrecimiento ofrecimiento;
	
	@OneToMany(cascade = CascadeType.ALL)
	private ArrayList<Notificacion> notificaciones = new ArrayList<Notificacion>(); 
	
	Match(){
		
	}
	
	public Match(Long id, Date fecha) {
		super();
		Id_Match = id;
		this.fecha = fecha;
	}

	public Long getId() {
		return Id_Match;
	}

	private void setId(Long id) {
		Id_Match = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
}
