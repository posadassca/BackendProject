package org.eclipse.DomainModel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Notificacion {

	@Id
	@GeneratedValue
	private Long Id_Notificacion;
	private String descripcion;
	
	@ManyToOne(mappedBy = "match", cascade = CascadeType.ALL)
	private Match match;
	
	Notificacion(){
		
	}

	public Notificacion(Long id, String descripcion) {
		super();
		Id_Notificacion = id;
		this.descripcion = descripcion;
	}

	public Long getId() {
		return Id_Notificacion;
	}

	private void setId(Long id) {
		Id_Notificacion = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
