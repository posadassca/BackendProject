package org.eclipse.DomainModel;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Musico {

	@Id
	@GeneratedValue
	private Integer Id_Musico;
	private String nombre;
	private String descripcion;
	
	@ManyToMany(cascade = CascadeType.ALL)
	private ArrayList<Banda> bandas = new ArrayList<Banda>();
	
	@OneToMany(cascade = CascadeType.ALL)
	private ArrayList<Ofrecimiento> ofrecimientos = new ArrayList<Ofrecimiento>();
	
	public Musico(){
		
	}

	public Musico(Integer id, String nombre, String descripcion) {
		super();
		Id_Musico = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return Id_Musico;
	}

	private void setId(Integer id) {
		Id_Musico = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public void agregarOfrecimiento(Ofrecimiento ofrecimiento){
		
		if (!ofrecimientos.contains(ofrecimiento)){					
			this.ofrecimientos.add(ofrecimiento);
			ofrecimiento.agregarMusico(this);
		}
	}
	
	public void eliminarOfrecimiento(Ofrecimiento ofrecimiento){
		
		if (ofrecimientos.contains(ofrecimiento)){
			this.bandas.remove(ofrecimiento);
			ofrecimiento.agregarMusico(null);
		}
	
	}
}
