package org.eclipse.DomainModel;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class GeneroMusical {

	@Id
	@GeneratedValue
	private Long Id_GeneroMusical;
	private String nombre;
	
	@ManyToMany(cascade = CascadeType.ALL)
	private ArrayList<Aviso> avisos = new ArrayList<Aviso>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	private ArrayList<Ofrecimiento> ofrecimientos = new ArrayList<Ofrecimiento>();
	
	GeneroMusical(){
		
	}

	public GeneroMusical(Long id, String nombre) {
		super();
		Id_GeneroMusical = id;
		this.nombre = nombre;
	}

	public Long getId() {
		return Id_GeneroMusical;
	}

	private void setId(Long id) {
		Id_GeneroMusical = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
