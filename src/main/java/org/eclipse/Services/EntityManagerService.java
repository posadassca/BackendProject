package org.eclipse.Services;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerService {
		
		private static EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("persistencia");
		
		private EntityManagerService() {			
		}
		
		public static EntityManager createEntityManager() {
		
			return entityManagerFactory.createEntityManager();
		}
	
}
