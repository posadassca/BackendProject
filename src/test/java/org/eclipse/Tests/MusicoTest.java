package org.eclipse.Tests;

import static org.junit.Assert.*;

import org.eclipse.DAOs.DAOJPA;
import org.eclipse.DAOs.MusicoDAO;
import org.eclipse.DomainModel.Musico;
import org.eclipse.DomainModel.Ofrecimiento;
import org.junit.Test;

public class MusicoTest {


	@Test
	public void test() {
		
		@SuppressWarnings("unused")
		MusicoDAO dao = (MusicoDAO) new DAOJPA<Musico>();

		Musico musico = new Musico(1, "sebas", "posadas");
		Ofrecimiento ofrecimiento = new Ofrecimiento(2,"Me ofre");
		
		musico.agregarOfrecimiento(ofrecimiento);
		
		dao.crear(musico);
		
		dao.cerrar();
	
	}

}
